" ==============
" Andreas' vimrc
" ==============

if &compatible
	" set nocompatible has side effects and should not be executed twice
	" this will make sure that is is used only when 'compatible' is set
	set nocompatible        " disable backwards compatibility with vi
endif


" self-install minipac
" ====================
"
" Inspired by https://github.com/k-takata/minpac/issues/85
"
if empty(glob('~/.config/nvim/pack/minpac/opt/minpac'))
	silent !git clone https://github.com/k-takata/minpac.git
		\ ~/.vim/pack/minpac/opt/minpac
	let s:minpac_first_install = 1
endif

" regular minpac setup
" ====================
"
packadd minpac
if !exists('*minpac#init')
	" minpac is not available.
	" settings for environment without plugins

else
	call minpac#init({'verbose':3})
	call minpac#add('k-takata/minpac', {'type': 'opt'}) " opt is required

	" other plugins here
	call minpac#add('altercation/vim-colors-solarized')
	call minpac#add('lifepillar/vim-solarized8')
    call minpac#add('dense-analysis/ale')
    call minpac#add('jiangmiao/auto-pairs')

	" actually install plugins
	if exists('s:minpac_first_install')
		call minpac#update()
	endif

	" plugin settings

	" minpac utility commands
	command! PackUpdate call minpac#update()
	command! PackClean call minpac#clean()
	command! PackStatus call minpac#status()

	" colors solarized
    " let g:solarized_use16=1
	set background=dark
	" colorscheme solarized
	colorscheme solarized8

    " ale
    let g:ale_python_pylint_executable = 'pylint3'
    let g:ale_linters = {
        \ 'markdown': ['proselint'],
        \ 'python': ['pylint'],
        \}
    let g:ale_fixers = {
        \ '*': ['remove_trailing_lines', 'trim_whitespace'],
        \ 'markdown': ['prettier'],
        \ 'python': ['isort', 'black'],
        \}
    let g:ale_fix_on_save = 1
endif

" common settings
" ===============

" key bindings
" jk is escape
inoremap jk <esc>

" visual behavior
syntax enable       " enable syntax highlighting
set showcmd		    " Show (partial) command in status line.
set showmatch		" Show matching brackets.
set cursorline		" show visual line under cursor position
set number          " show line numbers
set lazyredraw      " redraw only when needed, speeds up macros

" tab behavior optimized for python
set tabstop=4       " set tabs to have 4 spaces
set softtabstop=4   " number of spaces in tab when editing
set expandtab		" expand tabs into spaces

" indentation
filetype indent on  " load filetype specific indent files

" searching
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" UI
set wildmenu        " visual autocomplete for command menu
