# Changelog

Based on [Keep a Changelog](https://keepachangelog.com).

## [0.2.1] - 2020-04-26

### Added

- ALE asynchronous lint engine plugin
- auto-pairs plugin

## [0.2.0] - 2020-04-26

### Added

- Automatic installation of minpac
- Flexible setup with and without plugins
- Basic customization

### Changed

- Moved to solarized8 colorscheme

## [0.1.0] - 2020-04-25

### Added

- Initial version
