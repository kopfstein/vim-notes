================================
Vim notes - cheatsheet and vimrc
================================

My personal vim cheatsheet and vimrc.

There are many cheatsheets listing vim commands, some of them quite good. Neverheless, I decided to make my own because this looks like the best way to learn to me.


Getting help
============

:help *command*
	Find help on topic. Examples: :help w, :help c_CTRL-D, :help insert-index


Modes
=====

<esc>
	Switch to normal mode or cancels partially completed command.

<tab>, <ctrl>-d
	Command line completion, lists completion options.


Movement
========

h, j, k, l
	Cursor movement: left, down, up, right.

w, e
	Move to start of the next word, end of current word.

0, $
	Move to the beginning of the line, end of the line.

*number* *motion*
	Typing a number before a motion repeats it that many times.
	Example: 2w moves the cursor two words forward.
	Works also with operators. For instance, d2w deletes the two next words.

<ctrl>-G
	Show cursor position and file status.

G, gg
	Move to the bottom, start of the buffer.

*number* G
	Move to line number.


Insertion
=========

i, a, A
	Insert text at the cursor position, after the cursor, at the end of the line.

p
	Put text from register after cursor.
	Example: delete line with dd, move cursor, insert line with p.

o, O
	Open new line below cursor and enter insert mode, above cursor.


Deletion and replacement
========================

x
	Delete character under cursor.

d *motion*
	Deletion operating on *motion*. For instance, dw deletes until start of next word.

dd
	Delete current line.

r *character*, R *characters*
	Replace single character under the cursor, multiple characters.

c [number] *motion*
	Change operator, optionally repeated *number* times.
	Example: ce to change until end of the current word.


Visual mode
===========

v
	Start visual mode.

y
	Yank (copy) highlighted text.

p
	Paste text.


Undo
====

u, U
	Undo the last command, return the whole line to its original state.

<ctrl>-R
	Redo (undo the undo).


Searching and replacing
=======================

/ *phrase*, ? *phrase*
	Search forward for phrase in buffer, backwards.

n, N
	Search for same phrase again forward, backward.

<ctrl>-o
	Return to previous position after search (Ctrl and letter o).

%
	Find matching parentheses. Put cursor on parenthesis and press % to move to mattching parenthesis.

:s/*old*/*new*/g
	Substitute *new* for *old*. Adding the g flag means to subsitute globally in th eline, otherwise only first occurance is replaced.

:%s/*old*/*new*/gc
	Change every occurance in the whole buffer. Adding flag c to prompt whether to substitute.

:set hls
	Option hlsearch, highlighting of matches during search.

:set ic
	Option ignore case. To ignore case for just one search, use \c in the phrase: /ignore\c


Files, buffers, and external commands
=====================================

:q
	Quit vim. Use :q! to force quit without saving.

:! *command*
	Execute external command.
	Example: :!ls

:w *filename*
	Save buffers to *filename*. Also works on visual selection.

:r *filename*
	Insert contents from file. Can also be used to insert the output of external commands, example: :r!ls

:ls
	List buffers.

:bnext, :bprev, :bfirst, :blast
	Switch to the next, previous, first, and last buffer in the buffer list.

:bdelete N1 N2, :N,M bd
	Delete buffers N1 and N2, delete buffers N to M. Buffer numbers are listed by hte :ls command.


Windows
=======

<ctrl>-w v, <ctrl>-w h
	Splits window verically, horizontally.

<ctrl>-w w
	Change focus to next window.


Options
=======

:set *options*
	Set options.


References/acknowledgments
==========================

- Nice compact introduction to configuring vim: https://www.barbarianmeetscoding.com/blog/2018/10/24/exploring-vim-setting-up-your-vim-to-be-more-awesome-at-vim

- `Learn vimscript the hard way <https://learnvimscriptthehardway.stevelosh.com/>`
- `Solarized <https://ethanschoonover.com/solarized/>`
- Nice intro to vimrc `A good vimrc <https://dougblack.io/words/a-good-vimrc.html>`
- `Use vim as python IDE <http://liuchengxu.org/posts/use-vim-as-a-python-ide/>`

